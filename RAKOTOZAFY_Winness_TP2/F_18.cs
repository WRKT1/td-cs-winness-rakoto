﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public class F_18 : Spaceship, IAbility
    {
        public F_18() : base("F-18", 15, 0, false)
        {
        }

        public void UseAbility(List<Spaceship> spaceships)
        {
            int position = spaceships.IndexOf(this);
            int playerPosition = spaceships.IndexOf(spaceships.Where(x => x.BelongsPlayer).FirstOrDefault());
           
            if (Math.Abs(position - playerPosition) <= 1)
            {
                Console.WriteLine("Hello there... BOOM !");
                spaceships[playerPosition].TakeDamages(10);
                this.TakeDamages(1000000000);
            }
        }
    }
}
