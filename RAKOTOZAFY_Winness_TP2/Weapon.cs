﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public class Weapon : IWeapon
    {
        public string Name { get; set; }
        public EWeaponType WeaponType { get; set; }
        public double MinDamage { get; set; }
        public double MaxDamage { get; set; }
        public double AverageDamage { get => MinDamage + MaxDamage/2.0 ; }
        public double ReloadTime { get; set; }
        public double TimeBeforReload { get; set; }
        public bool IsReload => TimeBeforReload > 0;

        public Weapon(string name, double minDamage, double maxDamage, EWeaponType weaponType, double reloadTime)
        {
            Name = name;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            WeaponType = weaponType;
            ReloadTime = reloadTime;
            TimeBeforReload = reloadTime ;
        }

        public enum EWeaponType
        {
            Direct,
            Explosive,
            Guided
        }
        public double Shoot() 
        {
            if (!IsReload)
            {
                Console.WriteLine($"L'arme {Name} est en rechargement.. Attendez le prochain tour");
                return 0;
            }

            double damage = 0;
            switch(WeaponType)
            {
                case EWeaponType.Direct:
                    damage = DirectAttack();
                    break;
                case EWeaponType.Explosive:
                    damage = ExplosiveAttack();
                    break;
                case EWeaponType.Guided:
                    damage = GuidedAttack();
                    break;
            }
            if (TimeBeforReload <= 0) TimeBeforReload = ReloadTime;
            return damage;
         
        }
        private static bool RandomChance(double probability)
        {
            return new Random().NextDouble() < probability;
        }
        private double CalculateDamage()
        {
            return new Random().NextDouble() * (MaxDamage - MinDamage) + MinDamage;
        }
        private double DirectAttack()
        {
            if (RandomChance(0.1))
            {
                Console.WriteLine($"L'arme {Name} a raté sa cible.");
                return 0.0;
            }

            return CalculateDamage();
        }
        private double ExplosiveAttack()
        {
            if (RandomChance(0.25))
            {
                Console.WriteLine($"L'arme {Name} de type explosif a raté.");
                return 0.0;
            }

            ReloadTime *= 2;
            return CalculateDamage();
        }
        private double GuidedAttack()
        {
            Console.WriteLine($"L'arme {Name} de type guidé a touché.");
            return MinDamage;
        }
    }
}
