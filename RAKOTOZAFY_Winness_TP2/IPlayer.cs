﻿namespace RAKOTOZAFY_Winness_TP2
{
    public interface IPlayer
    {
        Spaceship DefaultSpaceship { get; set; }
        string Name { get; }
        string Alias { get; }
    }
}