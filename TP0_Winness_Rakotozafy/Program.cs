﻿using System;
using System.Linq;
using System.Threading;


namespace TP0_Winness_Rakotozafy
{
    class Program
    {
        // DEBUT Main
        static void Main(string[] args)
        {
            Salutations();
            Console.WriteLine();

            DonneesPersonnelles();
            Console.WriteLine();

            NombreCheveux();

            MenuProgramme();
        }
        // FIN Main
        static void Message(string message)
        {
            Console.WriteLine();
            Console.WriteLine(message);
        }
        static void MessageErreur(string message)
        {
            Console.WriteLine(">> (-_-) Erreur : {0}", message);
            Console.WriteLine();
        }
        static string NomComplet(string prenom, string nom)
        {
            return prenom.Substring(0, 1).ToUpper() + prenom.Substring(1).ToLower() + " " + nom.ToUpper();
        }
        static void Salutations()
        {
            Message(">>> Bienvenue sur mon programme, jeune étranger imberbe <<<");
            Console.WriteLine();
            bool conditionEstVrai = false;
            do
            {
                Console.Write("Donne moi ton nom, vil chenapan : ");
                string nom = Console.ReadLine();
                Console.Write("Et quel est ton prénom, petit galopin : ");
                string prenom = Console.ReadLine();

                if ((prenom.Any(char.IsDigit)) || (nom.Any(char.IsDigit)))
                {
                    MessageErreur("Arrête s'il te plait, ton nom ET ton prenom ne doivent pas être et contenir des chiffres.");
                }
                else
                {
                    conditionEstVrai = true;
                    Console.WriteLine();
                    Console.WriteLine("==> (^o^) Bonjour, {0} !", NomComplet(prenom, nom));
                }
            } while (!conditionEstVrai);

        }
        static void DonneesPersonnelles()
        {
            bool conditionEstVrai = false;
            int tailleCm;
            int age;
            float poids;
            do
            {
                Console.Write("Saisis ta taille (cm) : ");
                tailleCm = int.Parse(Console.ReadLine());

                Console.Write("Saisis ton poids (kg) : ");
                poids = float.Parse(Console.ReadLine());

                Console.Write("Saisis ton age : ");
                age = int.Parse(Console.ReadLine());

                ValidationNombre(tailleCm);
                ValidationNombre(age);
                ValidationNombre(poids);

                if (ValidationNombre(tailleCm) && ValidationNombre(age) && ValidationNombre(poids))
                {
                    if (age < 18)
                    {
                        Message("==> T'es un morveux puéril et immature qui n’a même pas le droit d’acheter de l’alcool en grande surface.");
                    }
                    else
                    {
                        Message("==> Vu que tu es majeur, tu peux boire autant que tu veux ! Mais attention à ton foie quand même ;)");
                    }

                    Console.WriteLine();
                    Console.WriteLine("INFO: Ton IMC est {0}", CalculIMC(tailleCm, poids).ToString("0.0"));
                    Console.WriteLine("==> Commentaire : {0}", CommentaireIMC(CalculIMC(tailleCm, poids)));

                    conditionEstVrai = true;
                }
                else
                {
                    MessageErreur("Arrête, tu ne peux pas avoir ni la taille, ni l'age, ni le poids en-dessous de 0. A moins que tu es une espèce rare que je ne connais pas encore...");
                }
            } while (!conditionEstVrai);

        }
        static float CalculIMC(int tailleCm, float poids)
        {
            float tailleM = (float)tailleCm / 100;
            return (float)(poids / Math.Pow(tailleM, 2));
        }
        static string CommentaireIMC(float valeurIMC)
        {
            const string INFERIEURE_16_5 = "Attention à l’anorexie !";
            const string ENTRE_16_5_18_5 = "Tu es un peu maigrichon !";
            const string ENTRE_18_5_25 = "Tu es de corpulence normale !";
            const string ENTRE_25_30 = "Tu es en surpoids !";
            const string ENTRE_30_35 = "Obésité modérée !";
            const string ENTRE_35_40 = "Obésité sévère !";
            const string SUPERIEUR_40 = "Obésité morbide !";

            if (valeurIMC < 16.5)
            {
                return INFERIEURE_16_5;
            }
            else if (valeurIMC < 18.5)
            {
                return ENTRE_16_5_18_5;
            }
            else if (valeurIMC < 25)
            {
                return ENTRE_18_5_25;
            }
            else if (valeurIMC < 30)
            {
                return ENTRE_25_30;
            }
            else if (valeurIMC < 35)
            {
                return ENTRE_30_35;
            }
            else if (valeurIMC < 40)
            {
                return ENTRE_35_40;
            }
            else if (valeurIMC >= 40)
            {
                return SUPERIEUR_40;
            }

            return null;
        }
        static void NombreCheveux()
        {
            Console.WriteLine("Parlons un peu de tes cheveux...");
            bool conditionEstVrai = false;
            while (!conditionEstVrai)
            {
                Console.Write("Dis-moi à peu près tu as combien de cheveux : ");
                string resultatEntree = Console.ReadLine(); 

                if (!int.TryParse(resultatEntree, out int resultatConversion) || (resultatConversion < 100000 || resultatConversion > 150000))
                {
                    MessageErreur("Valeur invalide. Réessaies.");
                }
                else
                {
                    conditionEstVrai = true;
                    Console.WriteLine("==> Annonce : Tu as {0} cheveux !!", resultatConversion);
                }

            }
        }
        static bool ValidationNombre(int nombre)
        {
            return nombre > 0;
        }
        static bool ValidationNombre(float nombre)
        {
  
            return nombre > 0;
        }
        static void MenuProgramme()
        {
            bool arretProgramme = false;

            do
            {
                Console.WriteLine();
                Console.WriteLine("Que veux-tu faire pour la suite : ");
                Console.WriteLine("1.Quitter le programme");
                Console.WriteLine("2.Recommencer le programme");
                Console.WriteLine("3.Compter jusqu'à 10");
                Console.WriteLine("4.Telephone à Tata Jacqueline");
                Console.WriteLine();
                Console.Write("Choisissez une option : ");

                int choixUtilisateur = int.Parse(Console.ReadLine());
                switch (choixUtilisateur)
                {
                    case 1:
                        arretProgramme = true;
                        QuitterProgramme();
                        break;
                    case 2:
                        RecommencerProgramme();
                        break;
                    case 3:
                        arretProgramme = true;
                        CompterDix();
                        break;
                    case 4:
                        arretProgramme = true;
                        TelephonerJacqueline();
                        break;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("(*-*) Tu te fous de moi ? CHOISIS ENTRE 1 ET 4 !!");
                        Thread.Sleep(1500);
                        break;
                }
            } while (!arretProgramme);
        }
        static void QuitterProgramme()
        {
            Console.WriteLine();
            Console.WriteLine(" (^o^) C'était un plaisir de t'accompagner sur ce programme. Au revoir.");
            Thread.Sleep(3000);
            return;
        }
        static void RecommencerProgramme()
        {
            Console.Clear();
            Salutations();
            Console.WriteLine();
            DonneesPersonnelles();
            Console.WriteLine();
            NombreCheveux();
        }
        static void CompterDix()
        {
            Message("Let's GO ! Lançons notre compte !!");
            for (int i = 1; i < 11; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            QuitterProgramme();
        }
        static void TelephonerJacqueline()
        {
            Message("Hello!! Vous êtes sur la boite vocale de Jacqueline, la tata d'enfer (^.<) ! Bon, malheureusement, je ne suis pas disponible pour le moment, mais si vous voulez me laissez un message, n'hésitez pas après le bip... BIIIIP !!!");
            Thread.Sleep(2000);
            QuitterProgramme();
        }
    }
}

