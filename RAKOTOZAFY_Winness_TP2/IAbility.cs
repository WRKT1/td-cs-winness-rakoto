﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public interface IAbility
    {
        void UseAbility(List<Spaceship> spaceships);
    }
}
