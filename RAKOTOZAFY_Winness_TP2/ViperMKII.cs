﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public class ViperMKII : Spaceship
    {
        private Weapon? lastUsedWeapon;
        public ViperMKII() : base("ViperMKII", 10, 15, true)
        {
            AddWeapon(Armory.Weapons[3]);
            AddWeapon(Armory.Weapons[4]);
            AddWeapon(Armory.Weapons[5]);
        }

        public override void ShootTarget(Spaceship target)
        {
            Weapon? selectedWeapon = Weapons.FirstOrDefault(w => w.IsReload == true && w != lastUsedWeapon);

            if (selectedWeapon != null)
            {
                double damage = selectedWeapon.Shoot();
                target.TakeDamages(damage);

                lastUsedWeapon = selectedWeapon;
            }
            else
            {
                Console.WriteLine("Aucune arme disponible pour le tir.");
            }

            foreach (Weapon weapon in Weapons)
            {
                weapon.TimeBeforReload = weapon.ReloadTime;
            }
        }
    }
}
