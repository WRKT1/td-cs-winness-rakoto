﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP1
{
    internal class SpaceInvaders
    {
        private List<Player> Players { get; set; } = new List<Player>();
        private List<Spaceship> Spaceships { get; set; } = new List<Spaceship>();
        public SpaceInvaders()
        {
            Init();
        }

        public static void Main()
        {
            Console.WriteLine("*** SPACE INVADERS ***");
            
            Armory armory = new Armory();
            SpaceInvaders spaceInvaders = new SpaceInvaders();
            
            foreach (Player player in spaceInvaders.Players)
            {
                Console.WriteLine();
                Console.Write("Joueur : ");
                Console.WriteLine(player.ToString());
                player.DefaultSpaceship.ViewShip();
            }

            Armory.ViewArmory();
        }
        private void Init()
        {
            // Creation trois modele de vaisseau
            Spaceships.Add(new Spaceship(100, 50));
            Spaceships.Add(new Spaceship(75, 75));

            Players.Add(new Player("toto", "bobo", "totocool75", Spaceships[0]));
            Players.Add(new Player("titi", "bibi", "titicool99", Spaceships[1]));
            Players.Add(new Player("tutu", "bubu", "tutucool69", Spaceships[0]));

            Spaceships[0].AddWeapon(Armory.Weapons[0]);
            Spaceships[0].AddWeapon(Armory.Weapons[2]);
            Spaceships[0].AddWeapon(Armory.Weapons[2]);
            Spaceships[1].AddWeapon(Armory.Weapons[1]);
            Spaceships[1].AddWeapon(Armory.Weapons[0]);
            Spaceships[1].AddWeapon(Armory.Weapons[0]);

        }

    }

}

