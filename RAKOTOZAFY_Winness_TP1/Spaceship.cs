﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP1
{
    internal class Spaceship
    {
        private int MaxStructure { get; }
        private int MaxShield { get; }
        private int CurrentStructure { get; set; }
        private int CurrentShield { get; set; }
        public bool IsDestroyed { get { return CurrentStructure <= 0; } }
        private List<Weapon> Weapons { get; } = new List<Weapon>();

        private const int MaxWeapons = 3;
        public Spaceship(int maxStructure, int maxShield)
        {
            MaxStructure = maxStructure;
            MaxShield = maxShield;
            CurrentStructure = maxStructure;
            CurrentShield = maxShield;
        }
        public void AddWeapon(Weapon weapon)
        {
            try
            {
                if (Weapons.Count < MaxWeapons)
                {
                    try
                    {
                        if (Armory.Weapons.Contains(weapon))
                        {
                            Weapons.Add(weapon);
                        }
                        else
                        {
                            throw new ArmoryException("L'arme n'est pas dans l'armurerie.");
                        }
                    }
                    catch (ArmoryException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    throw new InvalidOperationException("Le vaisseau spatial ne peut pas transporter plus de 3 armes.");
                }
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Erreur : " + ex.Message);
            }

        }

        public void RemoveWeapon(Weapon weapon)
        {
            try
            {
                if (Weapons.Contains(weapon))
                {
                    Weapons.Remove(weapon);
                }
                else
                {
                    throw new InvalidOperationException("Le vaisseau spatial ne contient pas l'arme en question.");
                }

            }
            catch (InvalidOperationException ex)
            {

                Console.WriteLine("Erreur : " + ex.Message);
            }
        }
        public void ClearWeapons()
        {
            Weapons.Clear();
        }
        public void ViewWeapons()
        {
            foreach (Weapon weapon in Weapons)
            {
                if (weapon != null)
                {
                    Console.WriteLine("Liste des armes du vaisseau :");
                    Console.WriteLine($"Nom : {weapon.Name}, Type : {weapon.WeaponType}, Dommage : {weapon.MinDamage}-{weapon.MaxDamage}");
                }
                else
                {
                    Console.WriteLine("Aucune arme à bord !");
                }
            }
        }

        public double AverageDamages()
        {
            if (Weapons.Count > 0)
            {
                int totalDamage = 0;
                foreach (Weapon weapon in Weapons)
                {
                    totalDamage += (weapon.MinDamage + weapon.MaxDamage) / 2;
                }
                return (double)totalDamage / Weapons.Count;
            }
            else
            {
                Console.WriteLine("Le vaisseau spatial ne contient aucune arme.");
                return 0.0;
            };
        }
        public void ViewShip()
        {
            Console.WriteLine("=== Vaisseau ===");
            Console.WriteLine($"Structure maximale : {MaxStructure}");
            Console.WriteLine($"Structure actuelle : {CurrentStructure}");
            Console.WriteLine($"Bouclier maximal : {MaxShield}");
            Console.WriteLine($"Bouclier actuel : {CurrentShield}");
            Console.WriteLine($"Vaisseau détruit : {IsDestroyed}");
            Console.WriteLine("=== Arme ===");
            ViewWeapons();
            Console.WriteLine("Degat moyenne : {0}", AverageDamages());
        }
    }
}
