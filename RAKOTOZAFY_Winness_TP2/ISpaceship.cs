﻿namespace RAKOTOZAFY_Winness_TP2
{
    public interface ISpaceship
    {
        string Name { get; set; }
        double MaxStructure {  get; set; }
        double MaxShield { get; set; }
        double CurrentStructure { get; set; }
        double CurrentShield { get; set; }
        bool IsDestroyed { get; }
        int MaxWeapons { get;  }
        bool BelongsPlayer { get; }
        List <Weapon> Weapons { get; }
        void AddWeapon(Weapon weapon);
        double AverageDamages();
        void TakeDamages(double damages);
        void RepairShield(double repair);
        void ShootTarget(Spaceship target);
        void ReloadWeapons();
        void RemoveWeapon(Weapon weapon);
        void ClearWeapons();
        void ViewShip();
        void ViewWeapons();
    }
}