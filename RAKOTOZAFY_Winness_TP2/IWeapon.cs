﻿namespace RAKOTOZAFY_Winness_TP2
{
    public interface IWeapon
    {
        string Name { get; set; }
        Weapon.EWeaponType WeaponType { get; set; }
        double MinDamage { get; set; }
        double MaxDamage { get; set; }
        double AverageDamage { get; }
        double ReloadTime { get; set; }
        double TimeBeforReload { get; set; }
        bool IsReload { get; }
        double Shoot();
    }
}