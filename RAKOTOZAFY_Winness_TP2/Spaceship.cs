﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public abstract class Spaceship : ISpaceship
    {
        public string Name { get; set; }
        public double MaxStructure { get; set; }
        public double MaxShield { get; set; }
        public double CurrentStructure { get; set; }
        public double CurrentShield { get; set; }
        public bool IsDestroyed => CurrentStructure <= 0;
        public bool BelongsPlayer { get; }
        public List<Weapon> Weapons { get; } = new List<Weapon>();

        public int MaxWeapons => 3;
        public Spaceship(string _name, double _maxStructure, double _maxShield, bool _belongsPlayer)
        {
            Name = _name;
            MaxStructure = _maxStructure;
            MaxShield = _maxShield;  
            CurrentStructure = _maxStructure;
            CurrentShield = _maxShield;
            BelongsPlayer = _belongsPlayer;
        }
       
        public virtual void ReloadWeapons()
        {
            foreach (Weapon weapon in Weapons)
            {
                weapon.TimeBeforReload--;
            }
        }
        public void AddWeapon(Weapon weapon)
        {
            try
            {
                if (Weapons.Count < MaxWeapons)
                {
                    try
                    {
                        if (Armory.Weapons.Contains(weapon))
                        {
                            Weapons.Add(weapon);
                        }
                        else
                        {
                            throw new ArmoryException("L'arme n'est pas dans l'armurerie.");
                        }
                    }
                    catch (ArmoryException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                else
                {
                    throw new InvalidOperationException("Le vaisseau spatial ne peut pas transporter plus de 3 armes.");
                }
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Erreur : " + ex.Message);
            }

        }
        public void RemoveWeapon(Weapon weapon)
        {
            try
            {
                if (Weapons.Contains(weapon))
                {
                    Weapons.Remove(weapon);
                }
                else
                {
                    throw new InvalidOperationException("Le vaisseau spatial ne contient pas l'arme en question.");
                }

            }
            catch (InvalidOperationException ex)
            {

                Console.WriteLine("Erreur : " + ex.Message);
            }
        }
        public void ClearWeapons()
        {
            Weapons.Clear();
        }
        public void ViewWeapons()
        {
            foreach (Weapon weapon in Weapons)
            {
                if (weapon != null)
                {
                    Console.WriteLine($"Nom : {weapon.Name}, Type : {weapon.WeaponType}, Dommage : {weapon.MinDamage}-{weapon.MaxDamage}");
                }
                else
                {
                    Console.WriteLine("Aucune arme à bord !");
                }
            }
        }
        public double AverageDamages()
        {
            if (Weapons.Count > 0)
            {
                double totalDamage = 0;
                foreach (Weapon weapon in Weapons)
                {
                    totalDamage += (weapon.MinDamage + weapon.MaxDamage) / 2;
                }
                return totalDamage / Weapons.Count;
            }
            else
            {
                Console.WriteLine("Le vaisseau spatial ne contient aucune arme.");
                return 0.0;
            };
        }
        public virtual void TakeDamages(double damages)
        {
            Console.WriteLine(Name + " encaisse " + damages + " de dommages");
            if (CurrentShield >= damages)
            {
                CurrentShield -= damages;
            }
            else
            {
                CurrentStructure -= (damages - CurrentShield);
                CurrentShield = 0;
            }
            Console.WriteLine("Boucliers restants : " + CurrentShield);
            Console.WriteLine("Structure restante : " + CurrentStructure);

            if (IsDestroyed) { Console.WriteLine(Name + " est détruit !"); }
        }
        public virtual void RepairShield(double repair)
        {
            CurrentShield += repair;
            if (CurrentShield > MaxShield) 
            { 
                CurrentShield = MaxShield; 
            }
        }
        public virtual void ShootTarget(Spaceship target)
        {
            List<Weapon> temp = Weapons.Where(x => x.IsReload).ToList();

            if (temp.Count == 0)
            {
                Console.WriteLine("Le " + Name + " n'a pas d'armes rechargées");
                return;
            }
            Weapon w = temp.Where(x => x.AverageDamage == temp.Max(y => y.AverageDamage)).FirstOrDefault();
            Console.WriteLine(Name + " tire sur " + target.Name + " avec l'arme " + w.Name);
            target.TakeDamages(w.Shoot());

        }
        public void ViewShip()
        {
            Console.WriteLine("=== Informations Vaisseau ===");
            Console.WriteLine($"Nom : {Name} ");
            Console.WriteLine($"PV restants : {CurrentStructure}");
            Console.WriteLine($"Bouclier : {CurrentShield}");
            Console.WriteLine("-->Arme");
            ViewWeapons();
            Console.WriteLine($"Degats moyens : {AverageDamages()}");
        }
    }
}
