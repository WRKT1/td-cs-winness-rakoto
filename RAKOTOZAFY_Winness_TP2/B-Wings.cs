﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RAKOTOZAFY_Winness_TP2.Weapon;

namespace RAKOTOZAFY_Winness_TP2
{
    public class BWings : Spaceship
    {
        public BWings() : base("B-Wings", 30, 0, false)
        {
            AddWeapon(Armory.Weapons[1]);
        }

        public override void ReloadWeapons()
        {
            foreach (Weapon weapon in Weapons)
            {
                if (weapon.WeaponType == EWeaponType.Explosive)
                {
                    weapon.TimeBeforReload = 1;
                    weapon.ReloadTime = 1;
                }
            }
        }
    }
}
