﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public class Rocinante : Spaceship
    {
        public Rocinante() : base("Rocinante", 3, 5, false)
        {
            AddWeapon(Armory.Weapons[2]);
        }
        private static bool RandomChance(double probability)
        {
            return new Random().NextDouble() < probability;
        }
        public override void TakeDamages(double damages)
        {
            const double DodgeChanceMultiplier = 0.5;

            if(RandomChance(DodgeChanceMultiplier))
            {
                Console.WriteLine($"{Name} a esquivé le tir! ");
            }
        }
    }
}
