﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    internal class ArmoryException : Exception
    {
        public ArmoryException(string message) : base(message)
        {
        }
        public ArmoryException(string message, Exception innerException)
        : base(message, innerException)
        {
        }
    }
}
