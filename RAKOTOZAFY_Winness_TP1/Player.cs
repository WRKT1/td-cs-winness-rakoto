﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP1
{
    internal class Player
    {
        private string FirstName { get; }
        private string LastName { get; }
        private string Alias { get; set; }
        public string Name { get; }
        public Spaceship DefaultSpaceship { get; set; }

        public Player(string firstName, string lastName, string alias, Spaceship defaultSpaceship)
        {
            FirstName = FormatName(firstName);
            LastName = FormatName(lastName);
            Alias = alias;
            Name = FirstName + " " + LastName;
            DefaultSpaceship = defaultSpaceship;
        }

        private static string FormatName(string name)
        {
            return name[..1].ToUpper() + name[1..].ToLower();
        }

        public override string ToString()
        {
            return $"{Alias} ({FirstName} {LastName})";
        }
        public override bool Equals(object? obj)
        {
            if (obj is Player otherPlayer)
            {
                return string.Equals(Alias, otherPlayer.Alias, StringComparison.OrdinalIgnoreCase);
            }
            return false;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }

    }
}
