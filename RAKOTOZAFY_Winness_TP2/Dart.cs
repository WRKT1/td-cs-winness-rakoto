﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RAKOTOZAFY_Winness_TP2.Weapon;

namespace RAKOTOZAFY_Winness_TP2
{
    public class Dart : Spaceship
    {
        public Dart() : base("Dart", 10, 3, false)
        {
            AddWeapon(Armory.Weapons[0]);
        }

        public override void ReloadWeapons()
        {
            foreach (Weapon weapon in Weapons)
            {
                if (weapon.WeaponType == EWeaponType.Direct)
                {
                    weapon.TimeBeforReload = 1;
                    weapon.ReloadTime = 1;
                }
            }
        }
    }
}
