﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP1
{
    public class Armory
    {
        public static List<Weapon> Weapons = new List<Weapon>();
        public Armory() 
        {
            Init();
        }
        private void Init()
        {
            Weapons.Add(new Weapon("Pew pew", 10, 20, Weapon.EWeaponType.Direct));
            Weapons.Add(new Weapon("Bang bang", 20, 80, Weapon.EWeaponType.Explosive));
            Weapons.Add(new Weapon("Bip boom", 30, 90, Weapon.EWeaponType.Guided));
        }
        public static void ViewArmory()
        {
            Console.WriteLine();
            Console.WriteLine("*** ARMORY ***");
            foreach (Weapon weapon in Weapons)
            {
                Console.WriteLine($"Name: {weapon.Name}, Type: {weapon.WeaponType}, Min Damage: {weapon.MinDamage}, Max Damage: {weapon.MaxDamage}");
            }
        }
    }
}
