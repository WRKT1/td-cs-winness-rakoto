﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    internal class SpaceInvaders
    {
        private List<Spaceship> Spaceships { get; set; } = new List<Spaceship>();
        private List<Player> Players { get; set; } = new List<Player>();
        private List<Spaceship> Enemies { get; set; } = new List<Spaceship>();
        public SpaceInvaders()
        {
            Init();
        }

        public static void Main()
        {
            Console.WriteLine("*** SPACE INVADERS ***");
            _ = new Armory();
            SpaceInvaders spaceInvaders = new();

           int nbTour = 0;
           while (spaceInvaders.Enemies.Any(x => !x.IsDestroyed) && spaceInvaders.Players.Any(x => !x.DefaultSpaceship.IsDestroyed))
            {
                nbTour++;
                Console.WriteLine();

                Console.WriteLine($"Tour {nbTour}");
                spaceInvaders.PlayRound();
            }

            Console.WriteLine("Fin de la partie.");
        }
        private void Init()
        {
            Enemies.Add(new Dart());
            Enemies.Add(new BWings());
            Enemies.Add(new Tardis());
            Enemies.Add(new Rocinante());
            Enemies.Add(new F_18());

            Players.Add(new Player("Wiwi", "Rakoto", "WRKT", new ViperMKII()));

            foreach (Player player in Players)
            {
                Spaceships.Add(player.DefaultSpaceship);
            }

            foreach (Spaceship enemy in Enemies)
            {
                Spaceships.Add(enemy);
            }

            foreach (Spaceship spaceship in Spaceships)
            {
                Console.WriteLine(spaceship.Name);
            }
            // A l'initialisation on melange la liste
            Random rng = new();
            Spaceships.Sort((x, y) => rng.Next(-1, 2));

        }

        private void PlayRound()
        {
            foreach (Spaceship spaceship in Spaceships)
            {
                // Utilisation Ability
                if (spaceship is IAbility ability)
                {
                    ability.UseAbility(Enemies);
                }
                // Heal si bouclier touché
                spaceship.CurrentShield = Math.Min(spaceship.MaxShield, spaceship.CurrentShield + 2);
            }
            foreach (Player player in Players)
            {
                // Tour Ennemie
                foreach (Spaceship enemy in Enemies)
                {
                    Console.WriteLine($"Tour de l'ennemi : {enemy.Name}");
                    if (!enemy.IsDestroyed)
                    {
                        enemy.ShootTarget(player.DefaultSpaceship);
                    }
                }

                // Tour du Joueur
                Console.WriteLine($"Tour du joueur : {player.Name}");

                List<Spaceship> livingEnemies = Enemies.Where(enemy => !enemy.IsDestroyed).ToList();

                if (livingEnemies.Count > 0)
                {
                    int randomIndex = new Random().Next(livingEnemies.Count);
                    Spaceship targetEnemy = livingEnemies[randomIndex];
                    player.DefaultSpaceship.ShootTarget(targetEnemy);
                }

            }

            ViewInformations();
        }

        private void ViewInformations()
        {
            Console.WriteLine("");
            Console.WriteLine("--- État des vaisseaux à la fin du tour ---");
            foreach (Spaceship spaceship in Spaceships)
            {
                Console.WriteLine($"Vaisseau  {spaceship.Name} :");
                Console.WriteLine($"  Structure : {spaceship.CurrentStructure}");
                Console.WriteLine($"  Bouclier : {spaceship.CurrentShield}");
                Console.WriteLine($"  Détuit : {spaceship.IsDestroyed}");
            }
        }
    }

}

