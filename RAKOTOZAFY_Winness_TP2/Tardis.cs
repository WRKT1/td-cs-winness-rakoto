﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public class Tardis : Spaceship, IAbility
    {
        public Tardis() : base("Tardis", 1, 0, false)
        {
        }

        public void UseAbility(List<Spaceship> spaceships)
        {
            if (spaceships.Count > 1)
            {
                Random random = new();
                int currentIndex = spaceships.IndexOf(this);

                int otherIndex;
                do
                {
                    otherIndex = random.Next(0, spaceships.Count);
                } while (otherIndex == currentIndex);

                Spaceship otherSpaceship = spaceships[otherIndex];

                spaceships.RemoveAt(currentIndex);
                spaceships.Insert(random.Next(0, spaceships.Count), this);

                Console.WriteLine($"{Name} déplace le vaisseau {otherSpaceship.Name} au hasard dans la liste.");
            }
            else
            {
                Console.WriteLine($"{Name} ne peut pas effectuer le déplacement, il n'y a pas assez de vaisseaux.");
            }
        }
        public override void TakeDamages(double damages)
        {
            base.TakeDamages(damages);
        }
    }
}
