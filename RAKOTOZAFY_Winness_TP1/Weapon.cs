﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP1
{
    public class Weapon
    {
        public string Name { get; }
        public int MinDamage { get; }
        public int MaxDamage { get; }
        public EWeaponType WeaponType { get; set; }
        public Weapon(string name, int minDamage, int maxDamage, EWeaponType weaponType)
        {
            Name = name;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
            WeaponType = weaponType;
        }

        public enum EWeaponType
        {
            Direct,
            Explosive,
            Guided
        }
    }
}
