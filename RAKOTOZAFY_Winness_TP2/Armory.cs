﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAKOTOZAFY_Winness_TP2
{
    public class Armory
    {
        public static List<Weapon> Weapons = new();
        public Armory() 
        {
            Init();
        }
        private static void Init()
        {
            Weapons.Add(new Weapon("Laser", 2, 3, Weapon.EWeaponType.Direct, 2));
            Weapons.Add(new Weapon("Hammer", 1, 8, Weapon.EWeaponType.Explosive, 1.5));
            Weapons.Add(new Weapon("Torpille", 3, 3, Weapon.EWeaponType.Guided, 2));
            Weapons.Add(new Weapon("Mitrailleuse", 6, 8, Weapon.EWeaponType.Guided,1));
            Weapons.Add(new Weapon("EMG", 1, 7, Weapon.EWeaponType.Guided, 1.5));
            Weapons.Add(new Weapon("Missile", 4, 100, Weapon.EWeaponType.Guided, 4));
        }
        public static void ViewArmory()
        {
            Console.WriteLine();
            Console.WriteLine("*** ARMORY ***");
            foreach (Weapon weapon in Weapons)
            {
                Console.WriteLine($"Name: {weapon.Name}, Type: {weapon.WeaponType}, Min Damage: {weapon.MinDamage}, Max Damage: {weapon.MaxDamage}");
            }
        }
    }
}
